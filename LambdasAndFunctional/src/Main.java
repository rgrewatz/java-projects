import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by rgrewatz on 1/26/2017.
 */
class LambdasAndFunctional {

    public static void test(Void v){
        System.out.println("test");
    }
    enum Operation {Add, Subtract, Multiply, Divide};

    public static void main(String[] argc){
        Consumer<Void> a = LambdasAndFunctional::test;
        a.accept(null);
        ArrayList<Integer> array = new ArrayList();
        Predicate<Integer> predicate = (Integer x) -> {
            System.out.println("Predicate param: " + x);
            return true;
        };
        Consumer<Integer> consumer = (Integer x) -> {
            System.out.println("Consume param: " + x);
        };
        Consumer<Void> voidConsumer = (Void) -> {
            System.out.println("Void Consumer");
        };
        predicate.test(65);
        consumer.accept(66);
        voidConsumer.accept(null);
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        array.add(5);
        Operation op = Operation.Add;
        TestInterface testInterface = setOp(op);
        System.out.println(op.name() + ": " + testInterface.doOperation(array));
        op = Operation.Subtract;
        testInterface = setOp(op);
        System.out.println(op.name() + ": " + testInterface.doOperation(array));
        op = Operation.Multiply;
        testInterface = setOp(op);
        System.out.println(op.name() + ": " + testInterface.doOperation(array));
        op = Operation.Divide;
        testInterface = setOp(op);
        System.out.println(op.name() + ": " + testInterface.doOperation(array));

        RunCodeBlock codeBlock = () -> {
            System.out.println("Running Code Block");
            System.out.println("Code Block Ending");
        };
        codeBlock.runCodeBlock();
        codeBlock = () -> {
            System.out.println("Running New Code Block");
            System.out.println("New Code Block Ending");
        };
        codeBlock.runCodeBlock();
    }

    private static TestInterface setOp(Operation op){
        TestInterface<Integer, Double> testInterface = null;
        switch(op){
            case Add:
                testInterface = (list) -> {
                    double result = 0;
                    for(Integer l : list){
                        result += l;
                    }
                    return result;
                };
                break;
            case Subtract:
                testInterface = (list) -> {
                    double result = 0;
                    for(Integer l : list){
                        result -= l;
                    }
                    return result;
                };
                break;
            case Multiply:
                testInterface = (list) -> {
                    double result = 0;
                    if(list != null && !list.isEmpty()) {
                        result = 1;
                        for (Integer l : list) {
                            result *= l;
                        }
                    }
                    return result;
                };
                break;
            case Divide:
                testInterface = (list) -> {
                    double result = 0;
                    if(list != null && !list.isEmpty()) {
                        result = list.get(0);
                        for (int i = 1; i < list.size(); i++) {
                            result /= list.get(i);
                        }
                    }
                    return result;
                };
                break;
        }
        return testInterface;
    }
}

interface TestInterface<Param, Return>{
    public Return doOperation(ArrayList<Param> list);
}
interface RunCodeBlock{
    public void runCodeBlock();
}
